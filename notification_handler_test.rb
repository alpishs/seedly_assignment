require 'test/unit'
require_relative 'notification_handler'

class NotificationHandlerTest < Test::Unit::TestCase

  test "will read json file for user_id hackamorevisiting and display correct sequence" do
    wanted_ordering = [["2019-11-21 08:45:03 +0000", "gratuitystopper answered a question"],
                       ["2019-11-21 08:45:17 +0000", "funeralpierce upvoted a question"],
                       ["2019-11-21 08:45:59 +0000", "backwarddusty and makerchorse commented on a question"],
                       ["2019-11-21 08:47:08 +0000", "makerchorse commented on a question"]]
    output = NotificationHandler.new.getNotificationsForUser(notifications: "notifications.json", user_id: "hackamorevisiting")
    assert_equal(output, wanted_ordering)
    assert_not_empty(output)
  end

  test "will return empty array [] in case passed user_id not present in json file" do
    output = NotificationHandler.new.getNotificationsForUser(notifications: "notifications.json", user_id: "dummy_user")
    assert_equal(output, [])
    assert_empty(output)
  end

  test "will return nil in case File Not Found or data not present" do
    output = NotificationHandler.new.getNotificationsForUser(notifications: "1notifications.json", user_id: "dummy_user")
    assert_equal(output, nil)
    assert_nil(output)
  end

end