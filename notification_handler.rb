require "json"
require 'date'
require 'benchmark'

class NotificationHandler

  #Method Definition
  def getNotificationsForUser(notifications: ARGV[0], user_id: ARGV[1])
    #ARGV[0] ==> is Input File
    #ARGV[1] ==> is user_id

    begin
      sorted_users = Hash.new
      exec_time = Benchmark.measure {
        users_to_display = Hash.new
        #predefined notification types
        notification_types = { 1 => "answered a question", 2 => "commented on a question", 3 => "upvoted a question" }
        #reading from json file
        json_from_file = File.read(notifications)
        #raise StandardError,"No data present" unless json_from_file.empty?
        #parsing json data
        data = JSON.parse(json_from_file)
        #fetching only data corresponds to given user_id to getNotificationsForUser
        fetched_data = data.select{|d| d["user_id"] == user_id &&  d["created_at"] = DateTime.strptime(d["created_at"].to_s, '%Q').to_time.to_s}.sort_by! { |date| date['created_at'] }

        notified_users = fetched_data.group_by{|user| [user["notification_type_id"], user["target_id"]]}

        notified_users.each do |user|
          #if block for taking merging conditions into consideration
          if user[1].length > 1
            user_names = []
            user[1].each { |data| user_names << data["sender_id"]}
            notification_type = notification_types[user[1][0]["notification_type_id"]] || ""
            users_to_display[user[1].last["created_at"]] = user_names.reverse.join(" and ") + " " + notification_type
          else
            #else block for single user display
            user_name = []
            user_name << user[1][0]["sender_id"]
            notification_type = notification_types[user[1][0]["notification_type_id"]] || ""
            users_to_display[user[1][0]["created_at"]] =  user_name[0] + " " + notification_type
          end
        end

        sorted_users = users_to_display.sort_by { |key, val| key }
        sorted_users.each do |user|
          puts "\n"
          puts user
        end
      }

      #Writing time execution of program to execution_time.txt
      file = File.open("execution_time.txt", "w")
      file.write(exec_time.real)
      return sorted_users
    rescue StandardError, IOError => e
      #some error occur, dir not writable etc.
    ensure
      file.close unless file.nil?
    end
  end

end

#Driver Code
handler = NotificationHandler.new
handler.getNotificationsForUser()